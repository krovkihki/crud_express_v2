import PostService from '../services/PostService.js';
import FileService from '../services/FileService.js';
class PostController {
    async create(request, response) {
        try {
            const post = await PostService.create(request.body, request.files.picture); // используем сервис PostService !! указываем конкретный объект или будет пиздец!!
            response.status(200).send(post);
        } catch (error) {
            response.status(500).json(error);
        }
    }
    async getALL(request, response) {
        try {
            const posts = await PostService.getALL(); // функция получения всех записей
            return response.status(200).json(posts);
        } catch (error) {
            response.status(500).json(error);
        }
    }
    async getOne(request, response) {
        try {
            const { id } = request.params;
            const post = await PostService.getOne(id); // да чо за ужас то
            return response.status(200).json(post);
        } catch (error) {
            response.status(500).json(error);
        }
    }
    async update(request, response) {
        try {
            const { id } = request.params;
            const updatedPost = await PostService.update(id, request.body);
            return response.status(200).json(updatedPost);
        } catch (error) {
            response.status(500).json(error);
        }
    }
    async delete(request, response) {
        try {
            const { id } = request.params;
            if (!id) {
                return response
                    .status(400)
                    .json({ message: 'id not required' });
            }
            await PostService.delete(id); // функция удаляет пост из базы данных
            return response.status(200).json({ message: 'post deleted' });
        } catch (error) {
            response.status(500).json(error.message);
        }
    }
}
export default new PostController(); // экспортируем контроллер
