import Router from 'express'
import PostController from '../controllers/PostController.js'

const router = new Router() // создаем экземпляр класса Router
// /posts - это маршрут в нашем случае название сущностьи /:id это параметр
router.post('/posts',PostController.create)// создаем пост
router.get('/posts',PostController.getALL) // получаем все посты
router.get('/posts/:id',PostController.getOne) // получаем пост по id
router.put('/posts/:id',PostController.update) // обновляем пост по id
router.delete('/posts/:id',PostController.delete) // удаляем пост по id
export default router // export