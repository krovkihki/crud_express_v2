import Post from '../models/Post.js'; //импортируем модель Post
// в сервисе мы не используем response/request
import FileService from './FileService.js';

class PostService {
    async create(post, picture) {
        const fileName = FileService.saveFile(picture);
        const createdPost = await Post.create({ ...post, picture: fileName });// заносим объект picture в пост, разварачивая пост ...post
        return createdPost;
    }
    async getALL() {
        const posts = await Post.find(); // функция получения всех записей
        return posts;
    }
    async getOne(id) {
        if (!id) {
            throw new Error('id not required');
        }
        const post = await Post.findById(id);
        return post;
    }

    async update(id, post) {
        if (!id) {
            throw new Error('id not required');
        }
        const updatedPost = await Post.findByIdAndUpdate(id, post, {
            new: true,
        }); // new:true указывает на то чтобы нам вернулась обновленная версия поста
        return updatedPost;
    }
    async delete(id) {
        if (!id) {
            throw new Error('id not required');
        }
        const post = await Post.findByIdAndDelete(id); // функция удаляет пост из базы данных
        return post;
    }
}

export default new PostService(); // экспортируем сервис Post
