import * as uuid from 'uuid';// устанавливаем uuid
import * as path from 'path';//стандартный модуль для работы с файлами

class FileService {
    saveFile(file) {
        try {
            const fileName = uuid.v4() + '.jpg';// генерируем имя файла с помощью модуля uuid
            const filePath = path.resolve('./static', fileName);// получаем путь к файлу
            file.mv(filePath);// передаем путь к файлу в объект file и файл перемещается по пути
            return fileName;
        } catch (error) { 
            console.log(error);
        }
    }
}

export default new FileService();
