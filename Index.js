import express, { json, response } from 'express';
import mongoose from 'mongoose';
import router from './routes/PostRoutes.js';
import fileUpload from 'express-fileupload';

import dotenv from 'dotenv';// импортим модуль env для работы с переменными окружения
dotenv.config()//обращаемся к конфигу dotenv без этого переменные не возьмутся :(

const app = express();

const PORT = process.env.PORT || 8000;

app.use(express.json());

app.use(express.static('static')) // используем для отдачи файлов клиенту

app.use(fileUpload({
})); //регистрируем миддлевайр для работы с файлами

app.use ('/api',router) //регистрируем роутер


async function startApp() { 
    try {
        await mongoose.connect(process.env.DB_URL, {
            useUnifiedTopology: true,
            useNewUrlParser: true,
        });
        app.listen(PORT, () =>
            console.log(`Server is running on port ${PORT}`)
        );
    } catch (error) {
        console.log(error);
    }
}

startApp();
